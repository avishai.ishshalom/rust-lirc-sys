# LIRC_Sys

[![Licence](https://img.shields.io/badge/license-LGPL_3-green.svg)](https://www.gnu.org/licenses/lgpl-3.0.en.html)

Rust FFI binding for the Linux LIRC kernel API.

## Licence

This code is licenced under LGPLv3.
[![Licence](https://www.gnu.org/graphics/lgplv3-147x51.png)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
