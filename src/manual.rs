/*
 *  lirc-sys — a Rust FFI binding for the lirc kernel API.
 *
 *  Copyright © 2019  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use generated;

#[inline] pub fn lirc_space(value: u32) -> u32 { (value & generated::LIRC_VALUE_MASK) | generated::LIRC_MODE2_SPACE }
#[inline] pub fn lirc_pulse(value: u32) -> u32 { (value & generated::LIRC_VALUE_MASK) | generated::LIRC_MODE2_PULSE }
#[inline] pub fn lirc_frequency(value: u32) -> u32 { (value & generated::LIRC_VALUE_MASK) | generated::LIRC_MODE2_FREQUENCY }
#[inline] pub fn lirc_timeout(value: u32) -> u32 { (value & generated::LIRC_VALUE_MASK) | generated::LIRC_MODE2_TIMEOUT }

#[inline] pub fn lirc_value(value: u32) -> u32 { value & generated::LIRC_VALUE_MASK }
#[inline] pub fn lirc_mode2(value: u32) -> u32 { value & generated::LIRC_MODE2_MASK }

#[inline] pub fn lirc_is_space(value: u32) -> bool { lirc_mode2(value) == generated::LIRC_MODE2_SPACE }
#[inline] pub fn lirc_is_pulse(value: u32) -> bool { lirc_mode2(value) == generated::LIRC_MODE2_PULSE }
#[inline] pub fn lirc_is_frequency(value: u32) -> bool { lirc_mode2(value) == generated::LIRC_MODE2_FREQUENCY }
#[inline] pub fn lirc_is_timeout(value: u32) -> bool { lirc_mode2(value) == generated::LIRC_MODE2_TIMEOUT }

#[inline] pub fn lirc_mode2send(value: u32) -> u32 { value }
#[inline] pub fn lirc_send2mode(value: u32) -> u32 { value }
#[inline] pub fn lirc_mode2rec(value: u32) -> u32 { value << 16 }
#[inline] pub fn lirc_rec2mode(value: u32) -> u32 { value >> 16 }

//#[inline] pub fn lirc_can_send_raw() -> bool { lirc_mode2send(generated::LIRC_MODE_RAW) }
//#[inline] pub fn lirc_can_send_pulse() -> bool { lirc_mode2send(generated::LIRC_MODE_PULSE) }
//#[inline] pub fn lirc_can_send_mode2() -> bool { lirc_mode2send(generated::LIRC_MODE_MODE2) }
//#[inline] pub fn lirc_can_send_lirccode() -> bool { lirc_mode2send(generated::LIRC_MODE_LIRCCODE) }
pub const LIRC_CAN_SEND_RAW: u32 = generated::LIRC_MODE_RAW;
pub const LIRC_CAN_SEND_PULSE: u32 = generated::LIRC_MODE_PULSE;
pub const LIRC_CAN_SEND_MODE2: u32 = generated::LIRC_MODE_MODE2;
pub const LIRC_CAN_SEND_LIRCCODE: u32 = generated::LIRC_MODE_LIRCCODE;

//#[inline] pub fn lirc_can_rec_raw() -> bool { lirc_mode2rec(generated::LIRC_MODE_RAW) }
//#[inline] pub fn lirc_can_rec_pulse() -> bool { lirc_mode2rec(generated::LIRC_MODE_PULSE) }
//#[inline] pub fn lirc_can_rec_mode2() -> bool { lirc_mode2rec(generated::LIRC_MODE_MODE2) }
//#[inline] pub fn lirc_can_rec_scancode() -> bool { lirc_mode2rec(generated::LIRC_MODE_SCANCODE) }
//#[inline] pub fn lirc_can_rec_lirccode() -> bool { lirc_mode2rec(generated::LIRC_MODE_LIRCCODE) }
pub const LIRC_CAN_REC_RAW: u32 = generated::LIRC_MODE_RAW << 16;
pub const LIRC_CAN_REC_PULSE: u32 = generated::LIRC_MODE_PULSE << 16;
pub const LIRC_CAN_REC_MODE2: u32 = generated::LIRC_MODE_MODE2 << 16;
pub const LIRC_CAN_REC_SCANCODE: u32 = generated::LIRC_MODE_SCANCODE << 16;
pub const LIRC_CAN_REC_LIRCCODE: u32 = generated::LIRC_MODE_LIRCCODE << 16;

//#[inline] pub fn lirc_can_rec_mask() -> bool { lirc_mode2rec(generated::LIRC_CAN_SEND_MASK) }
pub const LIRC_CAN_REC_MASK: u32 = generated::LIRC_CAN_SEND_MASK << 16;

#[inline] pub fn lirc_can_send(value: u32) -> u32 { value & generated::LIRC_CAN_SEND_MASK }
#[inline] pub fn lirc_can_rec(value: u32) -> u32 { value & LIRC_CAN_REC_MASK }

const LIRC_IOCTL_MAGIC: u8 = b'i';
ioctl_read!(ioctl_lirc_get_features, LIRC_IOCTL_MAGIC, 0x00, generated::__u32);
ioctl_read!(ioctl_lirc_get_send_mode, LIRC_IOCTL_MAGIC, 0x01, generated::__u32);
ioctl_read!(ioctl_lirc_get_rec_mode, LIRC_IOCTL_MAGIC, 0x02, generated::__u32);
ioctl_read!(ioctl_lirc_get_rec_resolution, LIRC_IOCTL_MAGIC, 0x07, generated::__u32);
ioctl_read!(ioctl_lirc_get_min_timeout, LIRC_IOCTL_MAGIC, 0x08, generated::__u32);
ioctl_read!(ioctl_lirc_get_max_timeout, LIRC_IOCTL_MAGIC, 0x09, generated::__u32);
ioctl_read!(ioctl_lirc_get_length, LIRC_IOCTL_MAGIC, 0x0f, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_send_mode, LIRC_IOCTL_MAGIC, 0x11, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_rec_mode, LIRC_IOCTL_MAGIC, 0x12, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_send_carrier, LIRC_IOCTL_MAGIC, 0x13, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_rec_carrier, LIRC_IOCTL_MAGIC, 0x14, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_send_duty_cycle, LIRC_IOCTL_MAGIC, 0x15, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_transmitter_mask, LIRC_IOCTL_MAGIC, 0x17, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_rec_timeout, LIRC_IOCTL_MAGIC, 0x18, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_rec_timeout_reports, LIRC_IOCTL_MAGIC, 0x19, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_measure_carrier_mode, LIRC_IOCTL_MAGIC, 0x1d, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_rec_carrier_range, LIRC_IOCTL_MAGIC, 0x1f, generated::__u32);
ioctl_write_ptr!(ioctl_lirc_set_wideband_receiver, LIRC_IOCTL_MAGIC, 0x23, generated::__u32);
ioctl_read!(ioctl_lirc_get_rec_timeout, LIRC_IOCTL_MAGIC, 0x24, generated::__u32);


#[cfg(test)]
mod test {
    use super::*;

    // TODO Are these tests just fatuous?

    quickcheck!{ fn prop_lirc_space(x: u32) -> bool { lirc_space(x) == x } }
    quickcheck!{ fn prop_lirc_pulse(x: u32) -> bool { lirc_pulse(x) == x | generated::LIRC_MODE2_PULSE } }
    quickcheck!{ fn prop_lirc_frequency(x: u32) -> bool { lirc_frequency(x) == x | generated::LIRC_MODE2_FREQUENCY } }
    quickcheck!{ fn prop_lirc_timeout(x: u32) -> bool { lirc_timeout(x) == x | generated::LIRC_MODE2_TIMEOUT } }

    quickcheck!{ fn prop_lirc_value(x: u32) -> bool { lirc_value(x) == x & generated::LIRC_VALUE_MASK} }
    quickcheck!{ fn prop_lirc_mode2(x: u32) -> bool { lirc_mode2(x) == x & generated::LIRC_MODE2_MASK} }

    quickcheck!{ fn prop_lirc_is_space(x: u32) -> bool { lirc_is_space((x & generated::LIRC_MODE2_MASK) | generated::LIRC_MODE2_SPACE) } }
    quickcheck!{ fn prop_lirc_is_pulse(x: u32) -> bool { lirc_is_pulse((x & generated::LIRC_MODE2_MASK) | generated::LIRC_MODE2_PULSE) } }
    quickcheck!{ fn prop_lirc_is_frequency(x: u32) -> bool { lirc_is_frequency((x & generated::LIRC_MODE2_MASK) | generated::LIRC_MODE2_FREQUENCY) } }
    quickcheck!{ fn prop_lirc_is_timeout(x: u32) -> bool { lirc_is_timeout((x & generated::LIRC_MODE2_MASK) | generated::LIRC_MODE2_TIMEOUT) } }

    quickcheck!{ fn prop_lirc_mode2send(x: u32) -> bool { lirc_mode2send(x) == x } }
    quickcheck!{ fn prop_lirc_send2mode(x: u32) -> bool { lirc_send2mode(x) == x } }
    quickcheck!{ fn prop_lirc_mode2rec(x: u32) -> bool { lirc_mode2rec(x) == x << 16 } }
    quickcheck!{ fn prop_lirc_rec2mode(x: u32) -> bool { lirc_rec2mode(x) == x >> 16 } }

    quickcheck!{ fn prop_lirc_can_send(x: u32) -> bool { lirc_can_send(x) == x & generated::LIRC_CAN_SEND_MASK} }
    quickcheck!{ fn prop_lirc_can_rec(x: u32) -> bool { lirc_can_rec(x) == x & LIRC_CAN_REC_MASK} }
}
